#!/usr/bin/env python
# -*- coding: utf-8 -*-

import numpy as np
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

import model 
from model import Model

sigmoid = T.nnet.sigmoid


class LSTMRNN(Model):

    def __init__(self, name, numvis, numhid, numlayers, numframes, output_type='real', dropout=0.0, numpy_rng=None, theano_rng=None):
        super(LSTMRNN, self).__init__(name=name)

        self.numvis = numvis
        self.numhid = numhid
        self.numlayers = numlayers
        self.numframes = numframes
        self.output_type = output_type
        self.dropout = dropout

        if not numpy_rng:
            self.numpy_rng = np.random.RandomState(1)
        else:
            self.numpy_rng = numpy_rng
        if not theano_rng:
            self.theano_rng = RandomStreams(1)
        else:
            self.theano_rng = theano_rng

        self.inputs = T.matrix(name='inputs')

        self.whh = [theano.shared(value=self.numpy_rng.uniform( low=-0.01, high=0.01, size=(self.numhid, self.numhid*4) ).astype(theano.config.floatX), name='whh'+str(k)) for k in range(self.numlayers)]
        self.whx = [theano.shared(value=self.numpy_rng.uniform( low=-0.01, high=0.01, size=(self.numhid, self.numvis) ).astype(theano.config.floatX), name='whx'+str(k)) for k in range(self.numlayers)]
        self.wxh = [theano.shared(value=self.numpy_rng.uniform( low=-0.01, high=0.01, size=(self.numvis, self.numhid*4) ).astype(theano.config.floatX), name='wxh'+str(0))]
        self.wxh = self.wxh + [theano.shared(value=self.numpy_rng.uniform( low=-0.01, high=0.01, size=(self.numhid, self.numhid*4)).astype(theano.config.floatX), name='wxh'+str(k)) for k in range(self.numlayers-1)]
        self.bx = theano.shared(value=np.zeros( self.numvis, dtype=theano.config.floatX), name='bx')
        self.bhid = [theano.shared(value=np.zeros(self.numhid*4, dtype=theano.config.floatX), name='bhid'+str(k)) for k in range(self.numlayers)]
        self.params = self.whh + self.whx + self.wxh + self.bhid + [self.bx]

        self._batchsize = self.inputs.shape[0]
        self._input_frames = self.inputs.reshape(( self._batchsize, self.inputs.shape[1] // self.numvis, self.numvis)).transpose(1, 0, 2)

        #1-step prediction --- 
        self.hids0_memstate = [T.zeros((self._batchsize, self.numhid))] * self.numlayers
        self.hids0_out = [T.zeros((self._batchsize, self.numhid))] * self.numlayers
        self.prehids1 = [T.dot(self.hids0_out[0], self.whh[0]) \
                          + self.bhid[0] + T.dot(self._input_frames[0], self.wxh[0])]
        self.hids1_fgate = T.nnet.sigmoid(self.prehids1[0][:, 0*self.numhid:1*self.numhid])
        self.hids1_igate = T.nnet.sigmoid(self.prehids1[0][:, 1*self.numhid:2*self.numhid])
        self.hids1_ogate = T.nnet.sigmoid(self.prehids1[0][:, 2*self.numhid:3*self.numhid])
        self.hids1_memstate = [self.hids1_fgate * self.hids0_memstate[0]
                               + self.hids1_igate * T.tanh(self.prehids1[0][:, 3*self.numhid:4*self.numhid])]
        #self.hids1_out = [T.tanh(self.hids_ogate * self.hids_memstate[0])]
        #self.hids1_out = [self.hids1_ogate * self.hids1_memstate[0]]
        self.hids1_out = [self.hids1_ogate * T.tanh(self.hids1_memstate[0])]
        for k in range(1, self.numlayers):
            self.prehids1.append(T.dot(self.hids0_out[k], self.whh[k]) 
                                    + self.bhid[k] + T.dot(self.hids0_out[k-1], self.wxh[k]))
            self.hids1_fgate = T.nnet.sigmoid(self.prehids1[-1][:, 0*self.numhid:1*self.numhid])
            self.hids1_igate = T.nnet.sigmoid(self.prehids1[-1][:, 1*self.numhid:2*self.numhid])
            self.hids1_ogate = T.nnet.sigmoid(self.prehids1[-1][:, 2*self.numhid:3*self.numhid])
            self.hids1_memstate.append(self.hids1_fgate * self.hids0_memstate[k-1]
                               + self.hids1_igate * T.tanh(self.prehids1[-1][:, 3*self.numhid:4*self.numhid]))
            self.hids1_out.append(self.hids1_ogate * T.tanh(self.hids1_memstate[k-1]))

        self.x_pred1 = self.bx 
        for k in range(self.numlayers):
            self.x_pred1 += T.dot(self.hids1_out[k], self.whx[k]) 
        self.hids1_out = T.concatenate(self.hids1_out, 1)
        #--- 1-step prediction 

        def step_dropout(x_gt_t, dropoutmask, x_tm1, hids_tm1_memstate, hids_tm1_out):
            hids_tm1_memstate = [hids_tm1_memstate[:,k*self.numhid:(k+1)*self.numhid] for k in range(self.numlayers)]
            hids_tm1_out = [hids_tm1_out[:,k*self.numhid:(k+1)*self.numhid] for k in range(self.numlayers)]
            prehids_t = [T.dot(hids_tm1_out[0], self.whh[0]) + self.bhid[0] + T.dot(x_gt_t, self.wxh[0])]
            hids_t_fgate = T.nnet.sigmoid(prehids_t[0][:, 0*self.numhid:1*self.numhid])
            hids_t_igate = T.nnet.sigmoid(prehids_t[0][:, 1*self.numhid:2*self.numhid])
            hids_t_ogate = T.nnet.sigmoid(prehids_t[0][:, 2*self.numhid:3*self.numhid])
            hids_t_memstate = [hids_t_fgate * hids_tm1_memstate[0]
                               + hids_t_igate * T.tanh(prehids_t[0][:, 3*self.numhid:4*self.numhid])]
            hids_t_out = [hids_t_ogate * T.tanh(hids_t_memstate[0])]
            for k in range(1, self.numlayers):
                prehids_t.append(T.dot(hids_tm1_out[k], self.whh[k]) 
                                 + self.bhid[k] 
                                 + T.dot(dropoutmask * hids_t_out[k-1], (1.0/self.dropout)*self.wxh[k]))
                hids_t_fgate = T.nnet.sigmoid(prehids_t[-1][:, 0*self.numhid:1*self.numhid])
                hids_t_igate = T.nnet.sigmoid(prehids_t[-1][:, 1*self.numhid:2*self.numhid])
                hids_t_ogate = T.nnet.sigmoid(prehids_t[-1][:, 2*self.numhid:3*self.numhid])
                hids_t_memstate.append(hids_t_fgate * hids_tm1_memstate[k]
                             + hids_t_igate * T.tanh(prehids_t[k-1][:, 3*self.numhid:4*self.numhid]))
                hids_t_out.append(hids_t_ogate * T.tanh(hids_t_memstate[k]))

            x_pred_t = self.bx
            for k in range(self.numlayers):
                x_pred_t += T.dot(hids_t_out[k], self.whx[k]) 

            return x_pred_t, T.concatenate((hids_t_memstate), 1), T.concatenate(hids_t_out, 1)

#        def step_nodropout(x_gt_t, x_tm1, hids_tm1):
#            hids_tm1 = [hids_tm1[:,k*self.numhid:(k+1)*self.numhid] for k in range(self.numlayers)]
#            prehids_t = [T.dot(hids_tm1[0], self.whh[0]) + self.bhid[0] + T.dot(x_gt_t, self.wxh[0])]
#            hids_t = [prehids_t[0] * (prehids_t[0] > 0)]
#            for k in range(1, self.numlayers):
#                prehids_t.append(T.dot(hids_tm1[k], self.whh[k]) + self.bhid[k] + T.dot(hids_t[k-1], self.wxh[k]))
#                hids_t.append(prehids_t[k] * (prehids_t[k] > 0))
#            x_pred_t = self.bx
#            for k in range(self.numlayers):
#                x_pred_t += T.dot(hids_t[k], self.whx[k]) 
#            return x_pred_t, T.concatenate(hids_t, 1)

        if self.dropout == 0.0:
            assert False, "no dropout not implemented"
        else:
            self._dropoutmask = theano_rng.binomial(size=(self.inputs.shape[1] // self.numvis, self._batchsize, self.numhid), n=1, p=self.dropout, dtype=theano.config.floatX) 
            (self._predictions, self.hids_memstate, self.hids_out), self.updates = theano.scan(
                                                        fn=step_dropout,
                                                        sequences=[self._input_frames, self._dropoutmask],
                                                        outputs_info=[self._input_frames[0], T.concatenate(self.hids0_memstate, 1), T.concatenate(self.hids0_out,1)])

        if self.output_type == 'real':
            self._prediction = self._predictions[:, :, :self.numvis]
        elif self.output_type == 'binary':
            self._prediction = sigmoid(self._predictions[:, :, :self.numvis])
        elif self.output_type == 'softmax':
            # softmax doesn't support 3d tensors, reshape batch and time axis
            # together, apply softmax and reshape back to 3d tensor
            self._prediction = T.nnet.softmax(
                self._predictions[:, :, :self.numvis].reshape((
                    self._predictions.shape[0] * self._predictions.shape[1],
                    self.numvis))).reshape((
                        self._predictions.shape[0], self._predictions.shape[1],
                        self.numvis))
        else:
            raise ValueError('unsupported output_type')

        self._prediction_for_training = self._prediction[:self.numframes-1]

        if self.output_type == 'real':
            self._cost = T.mean(( self._prediction_for_training - self._input_frames[1:self.numframes])**2)
            self._cost_varlen = T.mean(( self._prediction - self._input_frames[1:])**2)
        elif self.output_type == 'binary':
            self._cost = -T.mean( self._input_frames[1:self.numframes] * T.log(self._prediction_for_training) + (1.0 - self._input_frames[1:self.numframes]) * T.log( 1.0 - self._prediction))
            self._cost_varlen = -T.mean( self._input_frames[1:] * T.log(self._prediction_for_training) + (1.0 - self._input_frames[1:]) * T.log( 1.0 - self._prediction))
        elif self.output_type == 'softmax':
            self._cost = -T.mean(T.log( self._prediction_for_training) * self._input_frames[1:self.numframes])
            self._cost_varlen = -T.mean(T.log( self._prediction) * self._input_frames[1:])

        self._grads = T.grad(self._cost, self.params)

        self.inputs_var = T.fmatrix('inputs_var')
        self.nsteps = T.lscalar('nsteps')
        givens = {}
        givens[self.inputs] = T.concatenate(( self.inputs_var[:, :self.numvis], T.zeros(( self.inputs_var.shape[0], self.nsteps*self.numvis))), axis=1) 
        #self.predict = theano.function( [self.inputs_var, theano.Param( self.nsteps, default=self.numframes-4)], self._prediction.transpose(1, 0, 2).reshape(( self.inputs_var.shape[0], self.nsteps*self.numvis)), updates=self.updates, givens=givens)
        self.cost = theano.function( [self.inputs], self._cost, updates=self.updates)
        self.grads = theano.function( [self.inputs], self._grads, updates=self.updates)

    def grad(self, x):
        def get_cudandarray_value(x):
            if type(x) == theano.sandbox.cuda.CudaNdarray:
                return np.array(x.__array__()).flatten()
            else:
                return x.flatten()
        return np.concatenate([get_cudandarray_value(g) for g in self.grads(x)])

    def sample(self, numcases=1, numframes=10, temperature=1.0):
        assert self.output_type == 'softmax'
        next_prediction_and_state = theano.function([self._input_frames, self.hids_0], [self.theano_rng.multinomial(pvals=T.nnet.softmax(self.x_pred_1/temperature)), self.hids_1])
        preds = np.zeros((numcases, numframes, self.numvis), dtype="float32")
        preds[:,0,:] = self.numpy_rng.multinomial(numcases, pvals=np.ones(self.numvis)/np.float(self.numvis))
        hids = np.zeros((numcases, self.numhid*self.numlayers), dtype="float32")
        for t in range(1, numframes):
            nextpredandstate = next_prediction_and_state(preds[:,[t-1],:], hids)
            hids = nextpredandstate[1]
            preds[:,t,:] = nextpredandstate[0]
        return preds



