import os
HOME = os.environ['HOME']
import simplernn_scan 
import train

import pylab
import numpy
import numpy.random
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

numpy_rng  = numpy.random.RandomState(1)
theano_rng = RandomStreams(1)


def onehot(x,numclasses=None):                                                                                
    x = numpy.array(x)
    if x.shape==():                                                                                           
        x = x[numpy.newaxis]
    if numclasses is None:
        numclasses = x.max() + 1
    result = numpy.zeros(list(x.shape) + [numclasses],dtype=numpy.float32)
    z = numpy.zeros(x.shape)
    for c in range(numclasses):
        z *= 0
        z[numpy.where(x==c)] = 1
        result[...,c] += z
    return result                                                                                             


def unhot(labels):
    return labels.argmax(len(labels.shape)-1)                                                     


print 'loading data...'
import h5py
#f = h5py.File("./text9_withpunctuation_case__sentences.hdf5", "r")
f = h5py.File("./dummytest.hdf5", "r")
sentences = f["sentences"]
numsentences = len(sentences)
sentencelengths = f["sentencelengths"]
maxsentencelen = sentencelengths[:].max()
minsentencelen = sentencelengths[:].min()
alphabet = list(set(''.join(sentences[numpy_rng.binomial(1,1.0, numsentences).astype(bool)]))) #estimate alphabet from random chunk of the data
alphabetsize = len(alphabet)
dictionary = dict(zip(alphabet, range(alphabetsize)))
invdict = {v: k for k, v in dictionary.items()}
print '... done'
numtrain, numvalid = 10**6, 10**5

def datasource(f=f, numtrain=numtrain, numvalid=numvalid, maxloadsize=1000):
    randlen = numpy_rng.randint(minsentencelen, maxsentencelen) # pick a random sentence length each time 
    S = numpy.array([s for s in sentences[sentencelengths[:]==randlen]])
    S = S[numpy_rng.permutation(len(S))[:maxloadsize]]
    return onehot(numpy.array([[dictionary[c] for c in sentence] for sentence in S]), alphabetsize)


model = simplernn_scan.SRNN(name="aoeu", 
                                    numvis=alphabetsize,
                                    numhid=512, 
                                    numlayers=3, 
                                    dropout=0.5,
                                    output_type="softmax",
                                    numpy_rng=numpy_rng, 
                                    theano_rng=theano_rng)


# TRAIN MODEL
trainer = train.SGD_Trainer(model, datasource, batchsize=32, learningrate=0.01, loadsize=10000, gradient_clip_threshold=1.0)

print 'training...'
for epoch in xrange(10):
    trainer.step()
    print "valid cost:", model.cost(valid_features_numpy[:5000])


#print ''.join([invdict[unhot(a)] for a in model.sample(numframes=100)[0]])

