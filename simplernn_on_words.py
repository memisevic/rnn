import os
HOME = os.environ['HOME']
import simplernn_scan 
import train

import pylab
import numpy
import numpy.random
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

numpy_rng  = numpy.random.RandomState(1)
theano_rng = RandomStreams(1)


def onehot(x,numclasses=None):                                                                                
    x = numpy.array(x)
    if x.shape==():                                                                                           
        x = x[numpy.newaxis]
    if numclasses is None:
        numclasses = x.max() + 1
    result = numpy.zeros(list(x.shape) + [numclasses],dtype=numpy.float32)
    z = numpy.zeros(x.shape)
    for c in range(numclasses):
        z *= 0
        z[numpy.where(x==c)] = 1
        result[...,c] += z
    return result                                                                                             


def unhot(labels):
    return labels.argmax(len(labels.shape)-1)                                                     


#print 'loading data...'
#framelen = 1
#maxnumframes = 100
#traintext = open("./text8").readline()[:10000000]
#validtext = open("./text8").readline()[10000000:10100000]
##traintext = open("/data/text/enwik8").read()[:96000000]
##traintext = open("/data/text/enwik8").read()[:10000000]
##testtext = open("/data/text/enwik8").read()[96000000:]
##traintext = open("./linux_input.txt").read()
#
#alphabet = list(set(traintext))
#alphabetsize = len(alphabet)
#dictionary = dict(zip(alphabet, range(alphabetsize)))
#invdict = {v: k for k, v in dictionary.items()}
##numtrain, numvalid = maxnumframes*10000000, maxnumframes*10000
#traintext = traintext[:len(traintext)-len(traintext)%(alphabetsize*maxnumframes*framelen)]
#train_features_numpy = onehot(numpy.array([dictionary[c] for c in traintext])).reshape(-1, alphabetsize*maxnumframes*framelen)
#valid_features_numpy = onehot(numpy.array([dictionary[c] for c in validtext])).reshape(-1, alphabetsize*maxnumframes*framelen)
##valid_features_numpy = onehot(numpy.array([dictionary[c] for c in traintext[numtrain:numtrain+numvalid]])).reshape(-1, alphabetsize*maxnumframes*framelen)
#numcases = train_features_numpy.shape[0]
#del traintext 
#print '... done'

print '... loading data'
horizon = 15
vocabularysize = 10000
import gensim
from collections import Counter 
class MyCorpus(object):
    def __init__(self, corpusfile="./minitext8"):
        self.corpusfile = corpusfile

    def __iter__(self):
        word = ""
        corpus = open(self.corpusfile)
        while True:
            c = corpus.read(1)
            if c == "":
                break
            elif c == " ":
                if word != "":
                    yield word
                word = ""
                continue 
            else:
                word += c


textlen = 300000
minitext8 = []
for i, word in enumerate(MyCorpus(HOME+"/research/3rdparty/word2vec/trunk/text8")):
    #if word in stopwords:
    #    continue 
    minitext8.append(word)
    if i>textlen:
        break
minitext8file = open("minitext8", "w")
for word in minitext8:
    minitext8file.write(word + " ")
minitext8file.close()

corpus = MyCorpus()

most_common, num_occurrences = zip(*[w for w in Counter(corpus).most_common()[:vocabularysize]])
num_occurrences = numpy.array(num_occurrences, dtype=numpy.float32)

dictionary = gensim.corpora.Dictionary([word] for word in most_common)
tokensequence = numpy.array([dictionary.token2id[word] for word in corpus if word in most_common]).astype("int32")

train_features_numpy = numpy.vstack([onehot(tokensequence[i:i+horizon], vocabularysize).flatten() for i in range(0, tokensequence.shape[0]-horizon)])
#gap = 100 # take only every 100th subsequence, for now
#train_features_numpy = numpy.vstack([onehot(tokensequence[i:i+horizon], vocabularysize).flatten() for i in range(0, tokensequence.shape[0]-horizon, gap)])
numpy_rng.shuffle(train_features_numpy)
print 'done' 

#print [dictionary[i] for i in unhot(train_features_numpy[3].reshape(-1,vocabularysize))]

#numpy_rng.shuffle(valid_features_numpy)

model = simplernn_scan.SRNN(name="aoeu", 
                            numvis=vocabularysize,
                            numhid=512, 
                            numlayers=3, 
                            numframes=horizon, 
                            dropout=0.5,
                            output_type="softmax",
                            numpy_rng=numpy_rng, 
                            theano_rng=theano_rng)


# TRAIN MODEL
trainer = train.SGD_Trainer(model, train_features_numpy, batchsize=32, learningrate=0.01, loadsize=10000, gradient_clip_threshold=1.0)

print 'training...'
for epoch in xrange(100):
    trainer.step()
    #print "valid cost:", model.cost(valid_features_numpy[:5000])

#print [dictionary[i] for i in unhot(model.sample()[0])]



